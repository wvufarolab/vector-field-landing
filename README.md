## README ##

This repository gives a Matlab and Pyton implementation of the landing vector field described in the letter "Precise landing of autonomous aerial vehicles using vector fields" by Vinicius Mariano Goncalves, Ryan McLaughlin, and Guilherme A. S. Pereira.

Definition of parameters and function alpha are explained in the Letter.

### Who do I talk to? ###

* If you have any questions, please contact guilherme.pereira@mail.wvu.edu