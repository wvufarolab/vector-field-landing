% Vector Field
function [vx,vy,vz] = Field(A0,A1,B0,B1,C,K,x,y,z,alpha)
 r = sqrt(x^2+y^2);
 delta=0.01;
 H = Hnormal(A0,A1,B0,B1,C,K,r,z);
 pHpr = (Hnormal(A0,A1,B0,B1,C,K,r+delta,z)-H)/delta;
 pHpz = (Hnormal(A0,A1,B0,B1,C,K,r,z+delta)-H)/delta;
 a = alpha(x,y,z);
 vx = -x*a*pHpz/r;
 vy = -y*a*pHpz/r;
 vz =  a*pHpr;
end

% H function for normal approach
function y = Hnormal(A0,A1,B0,B1,C,K,r,z)
  D0 = (A0-B0)*C;
  D1 = (A1-B1)*C;
  U1 = (z/r-A0)/A1;
  U2 = (z-(B0*r+D0))/(B1*r+D1);
  U3 = z;
  y = maxK([U1 U2 U3],K);
end

% H function for shallow approach
function y = Hshallow(A0,A1,B1,C,K,r,z)
  D0 = (A0-B0)*C ;
  U1 = (z/r-A0)/A1;
  U2 = (z-(B0*r+D0))/(B1*r);
  U3 = z;
  y = maxK([minK([U1 U2],K),U3],K); 
end

% Max K function
function P = maxK(Q,K)
   mQ = max(Q);
   P = mQ+log(sum(exp(K*(Q-mQ))))/K;
end

% Min K function
function P = minK(Q,K)
   mQ = min(Q);
   P = mQ-log(sum(exp(-K*(Q-mQ))))/K;
end